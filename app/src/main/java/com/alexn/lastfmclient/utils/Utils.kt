package com.alexn.lastfmclient.utils


import com.alexn.lastfmclient.BuildConfig
import io.realm.RealmConfiguration

fun trackDuarationForamtatter(trackLength: String?): String? {
    val seconds = trackLength?.substring(trackLength.length - 2, trackLength.length)
    val minutes = trackLength?.substring(0, trackLength.length - 2)
    return "$minutes:$seconds"
}

fun configureRealm(): RealmConfiguration {
    return RealmConfiguration.Builder()
        .name("lastfm_db")
        .schemaVersion(BuildConfig.VERSION_CODE.toLong())
        .deleteRealmIfMigrationNeeded()
        .build()
}