package com.alexn.lastfmclient.base

import androidx.lifecycle.MediatorLiveData
import com.alexn.lastfmclient.base.util.SingleLiveEvent


open class BaseViewState {

    val loading = MediatorLiveData<Boolean>()
    val toast = SingleLiveEvent<String>()
    val showError = SingleLiveEvent<String>()
    val showSuccess = SingleLiveEvent<String>()

    init {
        loading.value = false
    }

}