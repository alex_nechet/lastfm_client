package com.alexn.lastfmclient.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.alexn.lastfmclient.base.BaseViewState
import com.alexn.lastfmclient.base.view_model.BaseViewModelFactory
import com.alexn.lastfmclient.base.view_model.RootBaseViewModel
import com.alexn.lastfmclient.provider.ToastProvider
import dagger.android.support.DaggerAppCompatDialogFragment
import javax.inject.Inject

abstract class RootBaseDialogFragment<VM : RootBaseViewModel<BaseViewState>, ParentVM : RootBaseViewModel<BaseViewState>,
        DataBinding : ViewDataBinding> : DaggerAppCompatDialogFragment() {

    lateinit var viewModel: VM
    lateinit var parentViewModel: ParentVM

    @Inject
    internal lateinit var vmFactory: BaseViewModelFactory

    @Inject
    lateinit var toastProvider: ToastProvider

    abstract val vmClass: Class<VM>
    abstract val parentVMClass: Class<ParentVM>
    abstract val layoutId: Int
    abstract val brRes: Int

    var binding: DataBinding? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(layoutInflater, layoutId, container, false)
        binding?.setVariable(brRes, viewModel)
        binding?.setLifecycleOwner(this)
        binding?.let { onBindingReady(it) }
        return binding?.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, vmFactory).get(vmClass)
        viewModel.state.toast.observe(this, Observer { message -> message?.let { toastProvider.showToast(it) } })
        activity?.let { ViewModelProviders.of(it, vmFactory).get(parentVMClass) }
    }

    open fun onBindingReady(binding: DataBinding) {

    }
}