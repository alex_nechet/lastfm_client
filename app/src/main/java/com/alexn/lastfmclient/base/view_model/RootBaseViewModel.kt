package com.alexn.lastfmclient.base.view_model

import androidx.lifecycle.ViewModel
import com.alexn.lastfmclient.base.BaseViewState
import io.reactivex.disposables.CompositeDisposable

abstract class RootBaseViewModel<out State : BaseViewState> : ViewModel() {
    abstract val state: State

    protected val disposer = CompositeDisposable()

    override fun onCleared() = disposer.dispose()

}