package com.alexn.lastfmclient.base.bindings

import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingConversion
import com.alexn.lastfmclient.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import io.reactivex.functions.Action

@BindingAdapter("android:visibility")
fun setVisibility(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter("app:image")
fun setIcon(view: ImageView?, url: String?) {

    view?.let {
        val requestOptions: RequestOptions = RequestOptions()
        requestOptions.error(R.drawable.ic_photo_black_24dp)
        requestOptions.placeholder(R.drawable.ic_photo_black_24dp)
        Glide.with(it)
            .load(url)
            .apply(requestOptions)
            .into(view)
    }
}

@BindingConversion
fun toOnClickListener(listener: Action?): View.OnClickListener? {
    return if (listener != null) {
        View.OnClickListener {
            try {
                listener.run()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    } else {
        null
    }
}