package com.alexn.lastfmclient.base.util


import com.alexn.lastfmclient.R
import com.alexn.lastfmclient.provider.ToastProvider
import java.io.IOException
import java.io.InterruptedIOException
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ErrorHandler @Inject constructor(private val toastProvider: ToastProvider) {

    var errorsCallback: ErrorsCallback? = null


    fun handleError(throwable: Throwable): Boolean {
        throwable.printStackTrace()
        return when (throwable) {
            is IOException -> {
                when (throwable) {
                    is ConnectException -> toastProvider.showToast(R.string.unable_to_communicate_to_server)
                    is SocketTimeoutException -> toastProvider.showToast(R.string.unable_to_communicate_to_server)
                    is SocketException -> toastProvider.showToast(R.string.unable_to_communicate_to_server)
                    !is InterruptedIOException -> toastProvider.showToast(R.string.no_internet_connection)
                }
                true
            }
            is JsonDataException -> {
                toastProvider.showToast(R.string.malformed_json)
                true
            }
            is ServerError -> {
                return when (throwable.code) {
                    500, 502 -> {
                        toastProvider.showToast(R.string.server_error)
                        true
                    }

                    else -> {
                        errorsCallback?.errorMessage(getServerErrorMessage(throwable))
                        false
                    }
                }
            }

            is FromServerError -> {
//                toastProvider.showToast(throwable.message)
                errorsCallback?.errorMessage(throwable.message)
                true
            }
            else -> false
        }
    }

    interface ErrorsCallback {
        fun errorMessage(errorMessage: String?)
    }
}

fun getServerErrorMessage(throwable: Throwable): String? {
    return throwable.message
}

class InsignificantError : RuntimeException {
    constructor(cause: Throwable) : super(cause)
    constructor() : super()
}


class ServerError(val code: Int) : RuntimeException("Unexpected server error with code: $code")

class FromServerError(override val message: String, cause: Throwable?) :
    RuntimeException(message, cause)

