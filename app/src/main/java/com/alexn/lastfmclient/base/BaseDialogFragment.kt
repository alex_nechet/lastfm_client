package com.alexn.lastfmclient.base

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.alexn.lastfmclient.base.fragment.RootBaseDialogFragment

import io.reactivex.disposables.CompositeDisposable


abstract class BaseDialogFragment<VM : BaseViewModel<BaseViewState>, ParentVM : BaseViewModel<BaseViewState>,
        DataBinding : ViewDataBinding> : RootBaseDialogFragment<VM, ParentVM, DataBinding>() {

    protected val disposer: CompositeDisposable = CompositeDisposable()


    override fun onDestroy() {
        super.onDestroy()
        disposer.dispose()
    }
}