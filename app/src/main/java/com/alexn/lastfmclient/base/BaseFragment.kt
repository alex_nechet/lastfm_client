package com.alexn.lastfmclient.base


import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.alexn.lastfmclient.base.fragment.RootBaseFragment
import com.alexn.lastfmclient.base.util.ErrorHandler
import io.reactivex.disposables.CompositeDisposable

abstract class BaseFragment<VM : BaseViewModel<BaseViewState>, ParentVM : BaseViewModel<BaseViewState>,
        DataBinding : ViewDataBinding> : RootBaseFragment<VM, ParentVM, DataBinding>(), ErrorHandler.ErrorsCallback {

    protected val disposer: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.errorHandler.errorsCallback = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.state.showError.observe(this, Observer { errorMessage(it) })
    }

    override fun errorMessage(errorMessage: String?) {
        errorMessage?.let { showErrorSnackBar(errorMessage) }
    }

    protected fun showErrorSnackBar(message: String?) = view?.let { snackbarProvider.showError(message ?: "", it) }

    protected fun showSuccessSnackBar(message: String?) = view?.let { snackbarProvider.showSuccess(message ?: "", it) }
}