package com.alexn.lastfmclient.base.util

import androidx.lifecycle.MediatorLiveData
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


fun <T> Observable<T>.applySchedulers(): Observable<T> {
    return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.applyBackgroundSchedulers(): Observable<T> {
    return subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
}

fun Completable.applySchedulers(): Completable {
    return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun <T> Single<T>.applySchedulers(): Single<T> {
    return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.observeLoadingWithSchedulers(loading: MediatorLiveData<Boolean>): Observable<T> {
    return applySchedulers()
            .doOnSubscribe { loading.value = true }
            .doFinally { loading.value = false }
}

fun <T> Single<T>.observeLoadingWithSchedulers(loading: MediatorLiveData<Boolean>): Single<T> {
    return applySchedulers()
            .doOnSubscribe { loading.value = true }
            .doFinally { loading.value = false }
}

fun Completable.observeLoadingWithSchedulers(loading: MediatorLiveData<Boolean>): Completable {
    return applySchedulers()
            .doOnSubscribe { loading.value = true }
            .doFinally { loading.value = false }
}

fun <T> Observable<T>.observeLoading(loading: MediatorLiveData<Boolean>): Observable<T> {
    return doOnSubscribe { loading.value = true }
            .doFinally { loading.value = false }
}
