package com.alexn.lastfmclient.base.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.alexn.lastfmclient.base.BaseViewState
import com.alexn.lastfmclient.base.view_model.BaseViewModelFactory
import com.alexn.lastfmclient.base.view_model.RootBaseViewModel
import com.alexn.lastfmclient.provider.SnackbarProvider
import com.alexn.lastfmclient.provider.ToastProvider
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class RootBaseActivity<VM : RootBaseViewModel<BaseViewState>, DataBinding : ViewDataBinding> :
    DaggerAppCompatActivity() {

    lateinit var viewModel: VM

    @Inject
    internal lateinit var vmFactory: BaseViewModelFactory

    @Inject
    lateinit var toastProvider: ToastProvider

    @Inject
    lateinit var snackbarProvider: SnackbarProvider

    abstract val vmClass: Class<VM>
    abstract val layoutId: Int
    abstract val brRes: Int

    var binding: DataBinding? = null

    open val inflate = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, vmFactory).get(vmClass)
        if (inflate) {
            binding = DataBindingUtil.setContentView(this, layoutId)
        }
        binding?.setVariable(brRes, viewModel)
        binding?.setLifecycleOwner(this)
        viewModel.state.toast.observe(this, Observer { toastProvider.showToast(it) })
    }
}