package com.alexn.lastfmclient.base

import android.util.Log
import com.alexn.lastfmclient.provider.SharedPrefsProvider
import com.alexn.lastfmclient.base.util.ErrorHandler
import com.alexn.lastfmclient.base.util.InsignificantError
import com.alexn.lastfmclient.base.view_model.RootBaseViewModel
import javax.inject.Inject

abstract class BaseViewModel<out ViewState : BaseViewState> : RootBaseViewModel<ViewState>() {

    @Inject
    lateinit var errorHandler: ErrorHandler

    @Inject
    lateinit var sharedPrefsProvider: SharedPrefsProvider

    protected fun handleError(throwable: Throwable?) {
        when {
            throwable is InsignificantError -> Log.w(javaClass.simpleName, "handledCommonError: ${throwable.cause}")
            throwable != null -> handleUncommonError(throwable)
            else -> Log.w(javaClass.simpleName, "handledCommonError: received null throwable")
        }
    }

    open fun handleUncommonError(throwable: Throwable?) {
        throwable?.let {
            Log.e(javaClass.simpleName, "Unhandled error: $throwable")
            throw throwable
        }
    }

    companion object {
        const val LIMIT = 50
    }
}