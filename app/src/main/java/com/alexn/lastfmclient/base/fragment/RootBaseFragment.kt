package com.alexn.lastfmclient.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProviders
import com.alexn.lastfmclient.base.BaseViewState
import com.alexn.lastfmclient.base.view_model.BaseViewModelFactory
import com.alexn.lastfmclient.base.view_model.RootBaseViewModel
import com.alexn.lastfmclient.provider.SnackbarProvider
import com.alexn.lastfmclient.provider.ToastProvider
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class RootBaseFragment<VM : RootBaseViewModel<BaseViewState>, ParentVM : RootBaseViewModel<BaseViewState>,
        DataBinding : ViewDataBinding> : DaggerFragment() {

    lateinit var viewModel: VM
    lateinit var parentViewModel: ParentVM

    @Inject
    internal lateinit var vmFactory: BaseViewModelFactory

    @Inject
    lateinit var toastProvider: ToastProvider

    @Inject
    lateinit var snackbarProvider: SnackbarProvider

    abstract val vmClass: Class<VM>
    abstract val parentVMClass: Class<ParentVM>
    abstract val layoutId: Int
    abstract val brRes: Int

    var binding: DataBinding? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(layoutInflater, layoutId, container, false)
        binding?.setVariable(brRes, viewModel)
        binding?.setLifecycleOwner(this)
        binding?.let { onBindingReady(it) }
        return binding?.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, vmFactory).get(vmClass)
        activity?.let { parentViewModel = ViewModelProviders.of(it, vmFactory).get(parentVMClass) }
    }

    open fun onBindingReady(binding: DataBinding) {

    }
}