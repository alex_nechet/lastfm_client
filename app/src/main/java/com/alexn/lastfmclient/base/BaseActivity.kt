package com.alexn.lastfmclient.base

import androidx.appcompat.widget.Toolbar
import androidx.databinding.ViewDataBinding
import com.alexn.lastfmclient.base.activity.RootBaseActivity
import io.reactivex.disposables.CompositeDisposable


abstract class BaseActivity<VM : BaseViewModel<BaseViewState>, DataBinding : ViewDataBinding> : RootBaseActivity<VM, DataBinding>(){

    fun setToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    fun setToolbarNoBackAction(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }
}
