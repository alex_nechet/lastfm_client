package com.alexn.lastfmclient.base.recycler_view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

class BindingViewHolder<out ViewBinding : ViewDataBinding> : RecyclerView.ViewHolder {

    val binding: ViewBinding

    constructor(resId: Int, parent: ViewGroup) : this(DataBindingUtil.inflate(LayoutInflater.from(parent.context), resId, parent, false))

    constructor(viewBinding: ViewBinding) : super(viewBinding.root) {
        this.binding = viewBinding
    }

}