package com.alexn.lastfmclient.di.modules


import android.util.Log
import com.alexn.lastfmclient.BuildConfig
import com.alexn.lastfmclient.network.Api
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Modifier

import javax.inject.Singleton


@Module
class NetworkModule {

    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        return GsonBuilder()
            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
            .serializeNulls()
            .setPrettyPrinting()
            .create()
    }

    @Provides
    @Singleton
    internal fun provideConverterFactory(gson: Gson): GsonConverterFactory = GsonConverterFactory.create(gson)

    @Provides
    @Singleton
    internal fun provideLoggingInterceptor(): HttpLoggingInterceptor? {
        return if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            loggingInterceptor
        } else {
            null
        }
    }

    @Provides
    @Singleton
    internal fun provideClient(
        loggingInterceptor: HttpLoggingInterceptor?
    ): OkHttpClient {
        val okhttpBuilder = OkHttpClient.Builder()
        loggingInterceptor?.let { okhttpBuilder.addInterceptor(loggingInterceptor) }
        return okhttpBuilder.build()
    }

    @Provides
    @Singleton
    internal fun provideRetrofitBuilder(
        gsonConverterFactory: GsonConverterFactory,
        client: OkHttpClient
    ): Retrofit.Builder {
        return Retrofit.Builder()
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(gsonConverterFactory)
    }

    @Provides
    @Singleton
    internal fun retrofitClient(retrofitBuilder: Retrofit.Builder): Retrofit {
        return retrofitBuilder.baseUrl(BuildConfig.apiEndpoint).build()
    }

    @Provides
    @Singleton
    internal fun provideApiService(retrofit: Retrofit): Api {
        return retrofit.create<Api>(Api::class.java)
    }

}