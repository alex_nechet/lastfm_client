package com.alexn.lastfmclient.di.modules


import androidx.lifecycle.ViewModel
import com.alexn.lastfmclient.ui.MainViewModel
import com.alexn.lastfmclient.di.annotations.ViewModelKey
import com.alexn.lastfmclient.ui.album_details.AlbumDetailsViewModel
import com.alexn.lastfmclient.ui.albums.AlbumsViewModel
import com.alexn.lastfmclient.ui.search.ArtistsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @ViewModelKey(value = MainViewModel::class)
    @IntoMap
    internal abstract fun mainVM(mainViewModel: MainViewModel): ViewModel

    @Binds
    @ViewModelKey(value = ArtistsViewModel::class)
    @IntoMap
    internal abstract fun artistsVM(artistsViewModel: ArtistsViewModel): ViewModel

    @Binds
    @ViewModelKey(value = AlbumsViewModel::class)
    @IntoMap
    internal abstract fun searchVM(albumsViewModel: AlbumsViewModel): ViewModel

    @Binds
    @ViewModelKey(value = AlbumDetailsViewModel::class)
    @IntoMap
    internal abstract fun albumDetailsVM(albumDetailsViewModel: AlbumDetailsViewModel): ViewModel

}