package com.alexn.lastfmclient.di.modules

import com.alexn.lastfmclient.di.scopes.ActivityScope
import com.alexn.lastfmclient.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentsModule::class])
    internal abstract fun mainActivity(): MainActivity
}