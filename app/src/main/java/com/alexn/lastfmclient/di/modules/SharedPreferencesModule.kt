package com.alexn.lastfmclient.di.modules

import com.alexn.lastfmclient.provider.SharedPrefsProvider
import com.alexn.lastfmclient.provider.SharedPrefsProviderImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class SharedPreferencesModule {

    @Binds
    @Singleton
    abstract fun bindSharedPreferences(prefs: SharedPrefsProviderImpl): SharedPrefsProvider
}
