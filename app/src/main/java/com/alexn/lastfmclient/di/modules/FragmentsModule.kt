package com.alexn.lastfmclient.di.modules

import com.alexn.lastfmclient.di.scopes.FragmentScope
import com.alexn.lastfmclient.ui.album_details.AlbumDetailsFragment
import com.alexn.lastfmclient.ui.favorites.FavoritesFragment
import com.alexn.lastfmclient.ui.albums.AlbumsFragment
import com.alexn.lastfmclient.ui.search.ArtistsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun favoritesFragment() : FavoritesFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun artistsFragment() : ArtistsFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun searchFragment() : AlbumsFragment

    @FragmentScope
    @ContributesAndroidInjector
    internal abstract fun albumDetails()  :AlbumDetailsFragment
}