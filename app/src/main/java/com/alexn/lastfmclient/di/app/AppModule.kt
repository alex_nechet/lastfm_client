package com.alexn.lastfmclient.di.app

import android.app.Application
import android.content.Context
import com.alexn.lastfmclient.di.modules.NetworkModule
import com.alexn.lastfmclient.di.modules.SharedPreferencesModule
import com.alexn.lastfmclient.di.modules.ViewModelModule
import com.alexn.lastfmclient.provider.SnackbarProvider
import dagger.Binds
import dagger.Module

@Module(includes = [ViewModelModule::class, NetworkModule::class, SharedPreferencesModule::class])
abstract class AppModule {

    @Binds
    abstract fun bindContext(application: Application): Context
}
