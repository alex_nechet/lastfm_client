package com.alexn.lastfmclient.ui.album_details.adapter

import com.alexn.lastfmclient.R
import com.alexn.lastfmclient.base.recycler_view.BindingRecyclerViewAdapter
import com.alexn.lastfmclient.base.recycler_view.BindingViewHolder
import com.alexn.lastfmclient.databinding.RvItemTrackBinding
import com.alexn.lastfmclient.network.model.Track

class TracksAdapter : BindingRecyclerViewAdapter<RvItemTrackBinding, Track>() {
    override val resId: Int = R.layout.rv_item_track

    override fun onBindViewHolder(holder: BindingViewHolder<RvItemTrackBinding>, position: Int) {
        holder.binding.item = getItem(position)
        holder.binding.position = position
    }

}