package com.alexn.lastfmclient.ui.albums

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import com.alexn.lastfmclient.base.BaseViewModel
import com.alexn.lastfmclient.base.BaseViewState
import com.alexn.lastfmclient.base.Paginator
import com.alexn.lastfmclient.base.util.SingleLiveEvent
import com.alexn.lastfmclient.base.util.applySchedulers
import com.alexn.lastfmclient.network.Api
import com.alexn.lastfmclient.network.model.Album
import com.alexn.lastfmclient.network.model.Album.Companion.ARTIST_NAME
import com.alexn.lastfmclient.ui.albums.adapter.AlbumsAdapter
import io.realm.Realm
import io.realm.RealmResults
import io.realm.kotlin.where
import javax.inject.Inject

class AlbumsViewModel @Inject constructor(private val api: Api) : BaseViewModel<SearchViewState>() {
    override val state: SearchViewState =
        SearchViewState()

    var artistName: String? = null
    val empty: ObservableBoolean = ObservableBoolean(true)

    val albumAdapterCallbacks: AlbumsAdapter.AlbumAdapterCallbacks = object : AlbumsAdapter.AlbumAdapterCallbacks {
        override fun onItemClick(item: Album) {
            state.navigateToAlbumDetails.value =
                Pair(
                    item.name,
                    if (!item.artistName.isNullOrEmpty()) item.artistName
                    else item.artistModel?.nameArtist
                )
        }
    }

    fun getFavoriteAlbums(): RealmResults<Album>? {
        val results = Realm.getDefaultInstance()?.where<Album>()?.sort(ARTIST_NAME)?.findAll()
        if (results.isNullOrEmpty()) empty.set(true) else empty.set(false)
        return results
    }

    val paginator = Paginator({ page ->
        api.getTopArtistAlbums(artistName, page, LIMIT)
            .applySchedulers()
            .filter { it.topalbums?.albums!!.isNotEmpty() }
            .map { it.topalbums?.albums }
    }, object : Paginator.ViewController<Album> {

        override fun showData(show: Boolean, data: List<Album>) {
            state.data.value = data
        }

        override fun showEmptyProgress(show: Boolean) {
            state.loading.value = show
        }

        override fun showEmptyError(show: Boolean, error: Throwable?) {
            state.showError.value = error?.localizedMessage
        }

        override fun showEmptyView(show: Boolean) {
            empty.set(show)
        }

        override fun showErrorMessage(error: Throwable) {
            state.showError.value = error.localizedMessage
        }

        override fun showRefreshProgress(show: Boolean) {

        }

        override fun showPageProgress(show: Boolean) {

        }
    })

    override fun onCleared() {
        super.onCleared()
        paginator.release()
    }

}

class SearchViewState : BaseViewState() {
    val data = MutableLiveData<List<Album>>()
    val navigateToAlbumDetails = SingleLiveEvent<Pair<String?, String?>>()
}
