package com.alexn.lastfmclient.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.alexn.lastfmclient.BR
import com.alexn.lastfmclient.R
import com.alexn.lastfmclient.base.BaseActivity
import com.alexn.lastfmclient.databinding.ActivityMainBinding


class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>() {
    override val vmClass: Class<MainViewModel> = MainViewModel::class.java
    override val layoutId: Int = R.layout.activity_main
    override val brRes: Int = BR.viewModel

    private fun navigateTo(id: Int) = findNavController(R.id.nav_host_fragment).navigate(id)

    override fun onSupportNavigateUp() = findNavController(R.id.nav_host_fragment).navigateUp()

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                findNavController(R.id.nav_host_fragment).navigateUp()
                return true
            }
            R.id.search -> {
                navigateTo(R.id.action_favoritesFragment_to_artistsFragment)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewModel.state.albumTitle.observe(this, Observer { supportActionBar?.title = it })
        viewModel.state.albumTitleRes.observe(this, Observer { actionBarTitle(it) })
    }

    private fun actionBarTitle(resId: Int){
        supportActionBar?.title = getString(resId)
    }


}
