package com.alexn.lastfmclient.ui.album_details

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.alexn.lastfmclient.BR
import com.alexn.lastfmclient.R
import com.alexn.lastfmclient.base.BaseFragment
import com.alexn.lastfmclient.databinding.FragmentAlbumDetailsBinding
import com.alexn.lastfmclient.ui.MainActivity
import com.alexn.lastfmclient.ui.MainViewModel
import com.alexn.lastfmclient.ui.album_details.adapter.TracksAdapter
import kotlinx.android.synthetic.main.fragment_album_details.*

class AlbumDetailsFragment : BaseFragment<AlbumDetailsViewModel, MainViewModel, FragmentAlbumDetailsBinding>() {
    override val vmClass: Class<AlbumDetailsViewModel> = AlbumDetailsViewModel::class.java
    override val parentVMClass: Class<MainViewModel> = MainViewModel::class.java
    override val layoutId: Int = R.layout.fragment_album_details
    override val brRes: Int = BR.viewModel

    private var tracksAdapter: TracksAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parentViewModel.state.albumTitleRes.value = R.string.album_details
        val albumName = arguments?.let { AlbumDetailsFragmentArgs.fromBundle(it).album }
        val artist = arguments?.let { AlbumDetailsFragmentArgs.fromBundle(it).artist }
        viewModel.setInitialData(artist, albumName)
        setAdapter()
        viewModel.state.tracks.observe(this, Observer { tracksAdapter?.submitList(it) })
    }

    private fun setAdapter() {
        tracksAdapter = TracksAdapter()
        val llm = LinearLayoutManager(context)
        recyclerView.layoutManager = llm
        recyclerView.adapter = tracksAdapter
    }


}