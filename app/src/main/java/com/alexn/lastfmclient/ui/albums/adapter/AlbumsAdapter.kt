package com.alexn.lastfmclient.ui.albums.adapter

import com.alexn.lastfmclient.R
import com.alexn.lastfmclient.base.recycler_view.BindingRecyclerViewAdapter
import com.alexn.lastfmclient.base.recycler_view.BindingViewHolder
import com.alexn.lastfmclient.databinding.RvItemAlbumBinding
import com.alexn.lastfmclient.network.model.Album
import com.alexn.lastfmclient.network.model.Album.Companion.MBID
import io.reactivex.functions.Action
import io.realm.Realm
import io.realm.kotlin.where

class AlbumsAdapter : BindingRecyclerViewAdapter<RvItemAlbumBinding, Album>() {
    override val resId: Int = R.layout.rv_item_album

    var callbacks: AlbumAdapterCallbacks? = null
    var isFavoritesFragment = false
    private val realm = Realm.getDefaultInstance()


    override fun onBindViewHolder(holder: BindingViewHolder<RvItemAlbumBinding>, position: Int) {
        holder.binding.item = orFromRealm(position, realm)
        holder.binding.artistName =
            if (getItem(position).artistName.isNullOrEmpty())
                getItem(position).artistModel?.nameArtist
            else
                getItem(position).artistName
        holder.binding.onItemClick = Action {
            orFromRealm(position, realm)?.let { callbacks?.onItemClick(it) }
        }
        holder.binding.favoritesCheckbox.setOnCheckedChangeListener { _, isChecked ->
            realm.executeTransaction {

                if (!isFavoritesFragment) {
                    getItem(position).artistName = getItem(position).artistModel?.nameArtist
                    getItem(position).isFavorite = isChecked
                }

                if (isChecked)
                    it.insertOrUpdate(getItem(position))
                else {
                    val query = findAlbumInRealm(position, it)
                    query?.deleteFromRealm()
                    if (isFavoritesFragment) notifyDataSetChanged()
                }
            }
        }
    }

    private fun orFromRealm(position: Int, realm: Realm) = findAlbumInRealm(position, realm) ?: getItem(position)

    private fun findAlbumInRealm(position: Int, realm: Realm) =
        realm.where<Album>().equalTo(MBID, getItem(position).mbid).and().equalTo("name", getItem(position).name).findFirst()

    interface AlbumAdapterCallbacks {
        fun onItemClick(item: Album)
    }

}