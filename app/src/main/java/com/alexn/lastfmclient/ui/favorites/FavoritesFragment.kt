package com.alexn.lastfmclient.ui.favorites

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.alexn.lastfmclient.R
import com.alexn.lastfmclient.ui.albums.BaseAlbumsFragment

class FavoritesFragment : BaseAlbumsFragment() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parentViewModel.state.albumTitleRes.value = R.string.favorite_albums
        setAdapter()
        albumsAdapter?.submitList(viewModel.getFavoriteAlbums())
        viewModel.state.navigateToAlbumDetails.observe(this, Observer {
            it?.let { pair ->
                val action = FavoritesFragmentDirections.actionFavoritesFragmentToAlbumDetailsFragment(pair.first!!, pair.second ?: "")
                findNavController().navigate(action)
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun setAdapter() {
        super.setAdapter()
        albumsAdapter?.isFavoritesFragment = true
    }



}