package com.alexn.lastfmclient.ui.search.adapter

import android.widget.BaseAdapter
import com.alexn.lastfmclient.R
import com.alexn.lastfmclient.base.recycler_view.BindingRecyclerViewAdapter
import com.alexn.lastfmclient.base.recycler_view.BindingViewHolder
import com.alexn.lastfmclient.databinding.RvItemArtistBinding
import com.alexn.lastfmclient.network.model.Artist
import io.reactivex.functions.Action

class ArtistsAdapter : BindingRecyclerViewAdapter<RvItemArtistBinding, Artist>() {
    override val resId: Int = R.layout.rv_item_artist

    var callbacks: ArtistsAdapterCallbacks? = null

    override fun onBindViewHolder(holder: BindingViewHolder<RvItemArtistBinding>, position: Int) {
        holder.binding.item = getItem(position)
        holder.binding.onItemClick = Action { callbacks?.onItemClick(getItem(position)) }
    }

    interface ArtistsAdapterCallbacks {
        fun onItemClick(item: Artist)
    }

}