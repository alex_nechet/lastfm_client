package com.alexn.lastfmclient.ui.search

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.alexn.lastfmclient.base.BaseViewModel
import com.alexn.lastfmclient.base.BaseViewState
import com.alexn.lastfmclient.base.Paginator
import com.alexn.lastfmclient.base.util.SingleLiveEvent
import com.alexn.lastfmclient.base.util.applySchedulers
import com.alexn.lastfmclient.network.Api
import com.alexn.lastfmclient.network.model.Artist
import com.alexn.lastfmclient.ui.search.adapter.ArtistsAdapter
import io.reactivex.functions.Action
import javax.inject.Inject

class ArtistsViewModel @Inject constructor(private val api: Api) : BaseViewModel<ArtistsViewState>() {
    override val state: ArtistsViewState = ArtistsViewState()

    val name: ObservableField<String> = ObservableField("")

    val artistsAdapterCallbacks: ArtistsAdapter.ArtistsAdapterCallbacks =
        object : ArtistsAdapter.ArtistsAdapterCallbacks {
            override fun onItemClick(item: Artist) {
                state.navigateToAlbums.value = item.nameArtist
            }
        }

    val paginator = Paginator({ page ->
        api.getArtistsList(name.get(), page, LIMIT)
            .applySchedulers()
            .map { it.results?.artistmatches?.artists }
    }, object : Paginator.ViewController<Artist> {

        override fun showData(show: Boolean, data: List<Artist>) {
            if (data.isNullOrEmpty()) showEmptyProgress(false)
            state.data.value = data
        }

        override fun showEmptyProgress(show: Boolean) {
            state.loading.value = show
        }

        override fun showEmptyError(show: Boolean, error: Throwable?) {
            state.showError.value = error?.localizedMessage
        }

        override fun showEmptyView(show: Boolean) {

        }


        override fun showErrorMessage(error: Throwable) {
            state.showError.value = error.localizedMessage
        }

        override fun showRefreshProgress(show: Boolean) {

        }

        override fun showPageProgress(show: Boolean) {

        }
    })

    fun onSearchClick() = Action {
        paginator.restart()
        paginator.refresh()
    }

    override fun onCleared() {
        super.onCleared()
        paginator.release()
    }
}

class ArtistsViewState : BaseViewState() {
    val data = MutableLiveData<List<Artist>>()
    val navigateToAlbums = SingleLiveEvent<String>()
}
