package com.alexn.lastfmclient.ui.albums

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.alexn.lastfmclient.BR
import com.alexn.lastfmclient.R
import com.alexn.lastfmclient.base.BaseFragment
import com.alexn.lastfmclient.ui.MainViewModel
import com.alexn.lastfmclient.ui.MainActivity
import com.alexn.lastfmclient.ui.albums.adapter.AlbumsAdapter
import com.alexn.lastfmclient.databinding.FragmentAlbumsBinding
import com.alexn.lastfmclient.network.model.Album
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.fragment_albums.*

open class AlbumsFragment : BaseAlbumsFragment() {
    override val vmClass: Class<AlbumsViewModel> = AlbumsViewModel::class.java
    override val parentVMClass: Class<MainViewModel> = MainViewModel::class.java
    override val layoutId: Int = R.layout.fragment_albums
    override val brRes: Int = BR.viewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val artist = arguments?.let { AlbumsFragmentArgs.fromBundle(it).artist }
        viewModel.artistName = artist
        parentViewModel.state.albumTitle.value = "${viewModel.artistName} ${getString(R.string.top_albums)}"
        setAdapter()
        viewModel.paginator.refresh()
        viewModel.state.data.observe(this, Observer { albumsAdapter?.submitList(it) })
        viewModel.state.navigateToAlbumDetails.observe(this, Observer {
            it?.let { pair ->
                val action = AlbumsFragmentDirections.actionAlbumsFragmentToAlbumDetailsFragment(pair.first!!, pair.second ?: "")
                findNavController().navigate(action)
            }
        })
    }

    override fun setAdapter() {
        super.setAdapter()
        recyclerView.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            viewModel.paginator.loadNewPage()
        }
    }


}