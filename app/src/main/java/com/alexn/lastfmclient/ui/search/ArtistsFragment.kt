package com.alexn.lastfmclient.ui.search

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.alexn.lastfmclient.BR
import com.alexn.lastfmclient.R
import com.alexn.lastfmclient.base.BaseFragment
import com.alexn.lastfmclient.ui.MainViewModel
import com.alexn.lastfmclient.databinding.FragmentArtistsBinding
import com.alexn.lastfmclient.ui.MainActivity
import com.alexn.lastfmclient.ui.albums.AlbumsFragment
import com.alexn.lastfmclient.ui.search.adapter.ArtistsAdapter
import kotlinx.android.synthetic.main.fragment_artists.*

class ArtistsFragment : BaseFragment<ArtistsViewModel, MainViewModel, FragmentArtistsBinding>() {
    override val vmClass: Class<ArtistsViewModel> = ArtistsViewModel::class.java
    override val parentVMClass: Class<MainViewModel> = MainViewModel::class.java
    override val layoutId: Int = R.layout.fragment_artists
    override val brRes: Int = BR.viewModel

    private var adapter: ArtistsAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parentViewModel.state.albumTitleRes.value = R.string.search_artist
        setAdapter()
        viewModel.state.data.observe(this, Observer { adapter?.submitList(it) })
        viewModel.state.navigateToAlbums.observe(this, Observer {
            val action = ArtistsFragmentDirections.actionArtistsFragmentToAlbumsFragment(it)
            findNavController().navigate(action)
        })
    }

    private fun setAdapter() {
        adapter = ArtistsAdapter()
        val llm = LinearLayoutManager(context)
        recyclerView.layoutManager = llm
        recyclerView.adapter = adapter
        adapter?.callbacks = viewModel.artistsAdapterCallbacks
        recyclerView.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY -> viewModel.paginator.loadNewPage() }
    }

    override fun onDestroy() {
        super.onDestroy()
        adapter?.callbacks = null
    }

}