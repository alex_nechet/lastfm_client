package com.alexn.lastfmclient.ui.album_details

import androidx.databinding.ObservableField
import com.alexn.lastfmclient.base.BaseViewModel
import com.alexn.lastfmclient.base.BaseViewState
import com.alexn.lastfmclient.base.util.ErrorHandler
import com.alexn.lastfmclient.base.util.SingleLiveEvent
import com.alexn.lastfmclient.base.util.observeLoadingWithSchedulers
import com.alexn.lastfmclient.network.Api
import com.alexn.lastfmclient.network.model.Album
import com.alexn.lastfmclient.network.model.AlbumInfo
import com.alexn.lastfmclient.network.model.Track
import javax.inject.Inject

class AlbumDetailsViewModel @Inject constructor(private var api: Api) : BaseViewModel<AlbumDetailsViewState>() {
    override val state: AlbumDetailsViewState = AlbumDetailsViewState()

    var album: ObservableField<AlbumInfo> = ObservableField(AlbumInfo())
    private var artist: String? = null
    private var albumName: String? = null

    fun setInitialData(artist: String?, albumName: String?) {
        this.artist = artist
        this.albumName = albumName
        getAlbumInfo(artist, albumName)
    }

    private fun getAlbumInfo(artist: String?, albumName: String?) {
        disposer.add(
            api.getAlbumInfo(artist, albumName)
                .observeLoadingWithSchedulers(state.loading)
                .subscribe({
                    album.set(it.album)
                    state.tracks.value = it.album?.tracks?.trackList
                }, { errorHandler.handleError(it) })
        )
    }

}

class AlbumDetailsViewState : BaseViewState() {
    val tracks = SingleLiveEvent<List<Track>>()
}
