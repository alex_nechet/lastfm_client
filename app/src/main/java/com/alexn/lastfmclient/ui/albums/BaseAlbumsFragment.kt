package com.alexn.lastfmclient.ui.albums

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.alexn.lastfmclient.BR
import com.alexn.lastfmclient.R
import com.alexn.lastfmclient.base.BaseFragment
import com.alexn.lastfmclient.databinding.FragmentAlbumsBinding
import com.alexn.lastfmclient.ui.MainViewModel
import com.alexn.lastfmclient.ui.albums.adapter.AlbumsAdapter
import kotlinx.android.synthetic.main.fragment_albums.*

abstract class BaseAlbumsFragment : BaseFragment<AlbumsViewModel, MainViewModel, FragmentAlbumsBinding>() {
    override val vmClass: Class<AlbumsViewModel> = AlbumsViewModel::class.java
    override val parentVMClass: Class<MainViewModel> = MainViewModel::class.java
    override val layoutId: Int = R.layout.fragment_albums
    override val brRes: Int = BR.viewModel

    protected var albumsAdapter: AlbumsAdapter? = null

    protected open fun setAdapter() {
        albumsAdapter = AlbumsAdapter()
        val llm = LinearLayoutManager(context)
        recyclerView.layoutManager = llm
        recyclerView.adapter = albumsAdapter
        albumsAdapter?.callbacks = viewModel.albumAdapterCallbacks
    }

    override fun onDestroy() {
        super.onDestroy()
        albumsAdapter?.callbacks = null
    }
}