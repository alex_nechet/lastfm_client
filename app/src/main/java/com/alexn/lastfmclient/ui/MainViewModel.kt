package com.alexn.lastfmclient.ui

import com.alexn.lastfmclient.base.BaseViewModel
import com.alexn.lastfmclient.base.BaseViewState
import com.alexn.lastfmclient.base.util.SingleLiveEvent
import javax.inject.Inject

class MainViewModel @Inject constructor(): BaseViewModel<MainViewState>(){
    override val state: MainViewState = MainViewState()

}

class MainViewState : BaseViewState(){
    val albumTitle = SingleLiveEvent<String>()
    val albumTitleRes = SingleLiveEvent<Int>()
}
