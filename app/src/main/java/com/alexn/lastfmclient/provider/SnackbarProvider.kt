package com.alexn.lastfmclient.provider

import android.view.View
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject
import javax.inject.Singleton
import android.widget.TextView
import com.alexn.lastfmclient.R


@Singleton
class SnackbarProvider @Inject constructor(private val resourceProvider: ResourceProvider) {

    fun showError(errorMessage: String?, view: View?) {
        makeSnackBar(view, errorMessage, R.color.snack_bar_error_color)
    }

    fun showSuccess(errorMessage: String?, view: View?) {
        makeSnackBar(view, errorMessage, R.color.snack_bar_success_color)
    }

    fun makeSnackBar(view: View?, errorMessage: String?, color: Int) {
        val snackBar = view?.let {
            errorMessage?.let { message ->
                Snackbar.make(it, message, Snackbar.LENGTH_LONG)
            }
        }
        snackBar?.view?.setBackgroundColor(resourceProvider.getColor(color))
        val textView = snackBar?.view?.findViewById(R.id.snackbar_text) as TextView
        textView.maxLines = 4
        snackBar?.show()
    }
}