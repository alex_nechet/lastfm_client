package com.alexn.lastfmclient.provider

import android.content.Context
import android.content.SharedPreferences
import com.alexn.lastfmclient.BuildConfig

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPrefsProviderImpl @Inject constructor(context: Context) : SharedPrefsProvider {

    companion object {
        const val API_KEY ="api_key"
    }
    private val sharedPreferences = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)

    override fun getSharedPrefs() : SharedPreferences = sharedPreferences

    override fun saveString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).commit()
    }


    override fun getString(key: String): String? = sharedPreferences.getString(key, null)

    override fun saveInt(key: String, value: Int) {
        sharedPreferences.edit().putInt(key, value).commit()
    }

    override fun getInt(key: String): Int = sharedPreferences.getInt(key, 0)

    override fun saveBoolean(key: String, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).commit()
    }

    override fun getBoolean(key: String): Boolean = sharedPreferences.getBoolean(key, false)


    override fun logOut() {
        sharedPreferences.edit().clear().commit()
    }


}