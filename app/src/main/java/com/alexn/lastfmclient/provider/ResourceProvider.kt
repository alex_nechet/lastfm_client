package com.alexn.lastfmclient.provider

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.TypedValue
import androidx.annotation.*
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import java.io.InputStream

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResourceProvider @Inject constructor(private val context: Context) {

    fun getString(@StringRes res: Int, vararg args: Any) = context.getString(res, *args)

    fun getStringArray(@ArrayRes res: Int) = context.resources.getStringArray(res)

    @ColorInt
    fun getColor(@ColorRes color: Int) = ContextCompat.getColor(context, color)

    fun getDrawable(@DrawableRes icon: Int) = AppCompatResources.getDrawable(context, icon)

    fun getRaw(@RawRes raw: Int) = context.resources.openRawResource(raw)

    fun getSP(value: Float): Float = getTypedValue(value, TypedValue.COMPLEX_UNIT_SP)

    fun getDP(value: Float): Float = getTypedValue(value, TypedValue.COMPLEX_UNIT_DIP)

    fun getTypedValue(value: Float, unit: Int) = TypedValue.applyDimension(unit, value, context.resources.displayMetrics)

    fun getDrawableId(resName: String, packageName: String) = context.resources.getIdentifier(resName, "drawable", packageName)
}
