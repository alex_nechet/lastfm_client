package com.alexn.lastfmclient.provider

import android.content.SharedPreferences

interface SharedPrefsProvider {

    fun saveString(key: String, value: String)

    fun getString(key: String): String?

    fun saveInt(key: String, value: Int)

    fun getInt(key: String): Int

    fun saveBoolean(key: String, value: Boolean)

    fun getBoolean(key: String): Boolean

    fun getSharedPrefs(): SharedPreferences?

    fun logOut()
}