package com.alexn.lastfmclient.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey

open class Album : RealmObject() {
    @Ignore
    @SerializedName("artist")
    var artistModel: Artist? = null

    var artistName: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("playcount")
    var playcount: Int? = null

    @PrimaryKey
    @SerializedName("mbid")
    var mbid: String? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("image")
    var images: RealmList<Image>? = null

    @Ignore
    @SerializedName("tracks")
    var tracks: Tracks? = null

    var isFavorite = false

    companion object {
        const val MBID ="mbid"
        const val ARTIST_NAME = "artistName"
    }
}

open class Image : RealmObject() {

    @PrimaryKey
    @SerializedName("#text")
    var text: String? = null

    @SerializedName("size")
    var size: String? = null
}

data class TopAlbumsResponse(
    @SerializedName("topalbums")
    var topalbums: Topalbums? = null
)

data class Topalbums(
    @SerializedName("album")
    var albums: List<Album>? = null
)