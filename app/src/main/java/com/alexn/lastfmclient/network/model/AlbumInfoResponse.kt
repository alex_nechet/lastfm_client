package com.alexn.lastfmclient.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey

data class AlbumInfoResponse(
    @SerializedName("album")
    var album: AlbumInfo? = null
)

class AlbumInfo {
    @SerializedName("artist")
    var artist: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("playcount")
    var playcount: Int? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("image")
    var images: RealmList<Image>? = null

    @SerializedName("tracks")
    var tracks: Tracks? = null

}

data class Track (

    @SerializedName("name")
    var nameTrack: String? = null,

    @SerializedName("url")
    var url: String? = null,

    @SerializedName("duration")
    var duration: String? = null,

    @SerializedName("artist")
    var artistTrack: Artist? = null
)


data class Tracks (
    @SerializedName("track")
    var trackList: RealmList<Track>? = null
)

