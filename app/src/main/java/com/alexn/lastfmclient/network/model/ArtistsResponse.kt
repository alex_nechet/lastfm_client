package com.alexn.lastfmclient.network.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.RealmResults
import io.realm.annotations.LinkingObjects
import io.realm.annotations.PrimaryKey

open class Artist : RealmModel {


    @SerializedName("name")
    var nameArtist: String? = null

    @SerializedName("listeners")
    var listeners: String? = null

//    @SerializedName("mbid")
//    var mbidArtist: String? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("streamable")
    var streamable: String? = null

    @SerializedName("image")
    var image: RealmList<Image>? = null

    @LinkingObjects("artist")
    val owners: RealmResults<Album>? = null
}


data class Artistmatches(
    @SerializedName("artist") var
    artists: List<Artist>? = null
)

data class ArtistsResponse(
    @SerializedName("results")
    var results: Results? = null
)

data class OpensearchQuery(
    @SerializedName("#text")
    var text: String? = null,

    @SerializedName("role")
    var role: String? = null,

    @SerializedName("searchTerms")
    var searchTerms: String? = null,

    @SerializedName("startPage")
    var startPage: String? = null
)

data class Results(
    @SerializedName("opensearch:Query")
    var opensearchQuery: OpensearchQuery? = null,

    @SerializedName("opensearch:totalResults")
    var opensearchTotalResults: String? = null,

    @SerializedName("opensearch:startIndex")
    var opensearchStartIndex: String? = null,

    @SerializedName("opensearch:itemsPerPage")
    var opensearchItemsPerPage: String? = null,

    @SerializedName("artistmatches")
    var artistmatches: Artistmatches? = null
)