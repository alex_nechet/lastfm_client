package com.alexn.lastfmclient.network

import com.alexn.lastfmclient.BuildConfig
import com.alexn.lastfmclient.network.model.Album
import com.alexn.lastfmclient.network.model.AlbumInfoResponse
import com.alexn.lastfmclient.network.model.ArtistsResponse
import com.alexn.lastfmclient.network.model.TopAlbumsResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

interface Api {

    @GET("?method=artist.search&format=json&api_key=${BuildConfig.API_KEY}")
    fun getArtistsList(
        @Query("artist") artist: String? = null,
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Observable<ArtistsResponse>

    @GET("?method=artist.gettopalbums&format=json&api_key=${BuildConfig.API_KEY}")
    fun getTopArtistAlbums(
        @Query("artist") artist: String? = null,
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Observable<TopAlbumsResponse>

    @GET("?method=album.getinfo&format=json&api_key=${BuildConfig.API_KEY}")
    fun getAlbumInfo(
        @Query("artist") artist: String? = null,
        @Query("album") albumName: String? = null
    ): Single<AlbumInfoResponse>


}