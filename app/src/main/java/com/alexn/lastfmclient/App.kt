package com.alexn.lastfmclient

import android.app.Activity
import android.app.Application

import com.alexn.lastfmclient.base.util.ErrorHandler
import com.alexn.lastfmclient.base.util.InsignificantError
import com.alexn.lastfmclient.di.app.DaggerAppComponent
import com.alexn.lastfmclient.utils.configureRealm

import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins
import io.realm.Realm

import javax.inject.Inject

open class App : Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var errorHandler: ErrorHandler

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)

        setUpErrorHandlers()
        Realm.init(this)
        Realm.setDefaultConfiguration(configureRealm())

    }

    private fun setUpErrorHandlers() {
        val defaultHandler = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler { thread, exception ->
            if (exception !is InsignificantError)
                defaultHandler.uncaughtException(thread, exception)
        }

        RxJavaPlugins.setErrorHandler {
            var exception = it
            if (exception is UndeliverableException) {
                exception = exception.cause
            }
            if (!errorHandler.handleError(exception)) {
                defaultHandler.uncaughtException(Thread.currentThread(), exception)
            }
        }
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector
}